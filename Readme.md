## Backbase Task 2: Codility Assessment
Since the codility setup was incorrect and it was not possible to run the tests, I have included the correct task result here.  
For simplicity, I have only included the relevant files and not the entire project.

## Original Task Description

Your task is to implement Angular Routing Logic that will handle the routing, redirecting and protection of pages.
1. Your task is to modify two files:
   `routes.ts`: a routing object with specific redirects to given components and protected pages.
   `GuardService.ts`: a Guard Service implementing a CanActivate Interface to protect pages and perform specific redirects
2. The app uses a token in localStorage, available under the key token.
   You can assume that, when the token is a non-empty string of any length, the user in the app is authenticated. When the token is an empty string, it means that the user is not authenticated
3. Four view components are available. They can all be imported from the . `/components` path:
    - PublicComponent: a public component page accessible under the /public route. When the URL is `/` it should also redirect to the `/public` URL. The page is accessible regardless of the authentication status in the app (that is, it's always accessible).
    - LoginComponent: a component page accessible under the `/login` route. When the user is already authenticated, it should redirect to the `/dashboard` URL. Also. if the user is not authenticated and tries to access the protected URL (`/dashboard`), a redirect to login should occur. 
    - DashboardComponent: a component page accessible under the `/dashboard` route. The logic should be the reverse of the previously mentioned `/login` route, i.e. if the user is not authenticated, it should redirect to the `/login` route. 
    - Component404: every attempt to access any route that was not mentioned above should result in a redirect to this component
   
Assumptions

- Design/styling is not assessed and will not affect the score. You should focus only on implementing the requirements.
- The level of typing precision is not assessed
- You should modify both files in order to achieve a 100% test score
- You can use console. log for debugging purposes via your browser's developer tools.
- Check unit tests' results for debugging purposes.
- The only available imports are available at the top of the files: `@angular/core(v.10.1.0)`, `@angular/router(v.10.1.6)` and `../components`
- You should not change the way routes is named and exported.
- You should not change the way GuardService is named and exported
   
Hints
- Implement the CanActivate interface for GuardService.
- Use `RouterStateSnapshot` to make redirects inside `GuardService` and check the URLs to which the user navigates
- Use `localStorage` `getItem` and `setItem` methods.
- Use the value `full` for `pathMatch` inside every route registered in routes.
- You do not have to worry about registering the routes object; it is done out of the box.

