import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';

@Injectable()
export class GuardService implements CanActivate {

  constructor(private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    let url: string = state.url;
    const token = localStorage.getItem('token');
    const isAuthenticated = !!token;

    if (url.includes('/login')) {
      if (!isAuthenticated) return true;
      this.router.navigate(['/dashboard']);
      return false;
    }

    if (url.includes('/dashboard')) {
      if (isAuthenticated) return true;
      this.router.navigate(['/login']);
      return false;
    }

    return false;
  }
}
