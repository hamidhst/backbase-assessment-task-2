import {Routes} from '@angular/router';
import {PublicComponent, LoginComponent, DashboardComponent, Component404} from '../components';
import {GuardService} from "./GuardService";

export const routes: Routes = [
  {
    path: '',
    redirectTo: '/public',
    pathMatch: 'full',
  },
  {
    path: 'public',
    component: PublicComponent,
    pathMatch: 'full',
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [GuardService],
    pathMatch: 'full',
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [GuardService],
    pathMatch: 'full',
  },
  {
    path: '**',
    component: Component404
  }
];
